use crate::models::Period;
use yew::prelude::*;

#[derive(PartialEq, Properties)]
pub struct PeriodComponentProps {
    pub period: Period,
}

#[function_component(PeriodComponent)]
pub fn period_component(props: &PeriodComponentProps) -> Html {
    html! {


    <div class="bg-white shadow-lg px-4 py-5 sm:px-6 flex flex-col gap-4">
        <div class="flex space-x-3">
          <div class="flex-shrink-0">
            <img class="h-20 w-20 rounded" src={props.period.icon.to_owned()} alt=""/>
          </div>
          <div class="min-w-0 flex-1">
            <p class="text-sm font-semibold text-gray-900">
              <p >{ props.period.name.to_owned()}</p>
            </p>
            <p class="text-sm text-gray-500">
              <p >{ props.period.start_time.to_owned()}</p>
            </p>
          </div>

        </div>

        <div class="text-gray-400">
            <p>{props.period.detailed_forecast.to_owned()}</p>
        </div>

      </div>

    }
}
