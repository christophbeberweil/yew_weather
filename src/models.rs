use serde::{Deserialize, Serialize};




#[derive(PartialEq, Debug, Serialize, Deserialize, Clone)]
#[serde(rename_all = "camelCase")]
pub struct Period {
    pub name: String,
    pub start_time: String,
    pub end_time: String,
    pub is_daytime: bool,
    pub temperature: f32,
    pub temperature_unit: String,
    pub wind_speed: String,
    pub wind_direction: String,
    pub icon: String,
    pub short_forecast: String,
    pub detailed_forecast: String,
}

#[derive(PartialEq, Debug, Serialize, Deserialize, Clone)]
pub struct Properties {
    pub periods: Vec<Period>,
}

#[derive(PartialEq, Debug, Serialize, Deserialize, Clone)]
pub struct Forecast {
    pub properties: Properties,
}
