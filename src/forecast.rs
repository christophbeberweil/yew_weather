use yew::prelude::*;

use crate::{models::Forecast, period::PeriodComponent};

#[derive(PartialEq, Properties)]
pub struct ForecastComponentProps {
    pub forecast: Forecast,
}

#[function_component(ForecastComponent)]
pub fn forecast_component(props: &ForecastComponentProps) -> Html {
    props
        .forecast
        .properties
        .periods
        .iter()
        .map(|period| {
            html! {
                <PeriodComponent period={period.clone()}/>
            }
        })
        .collect()
}
