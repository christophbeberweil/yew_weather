use reqwasm::{http::Request, Error};
use yew::prelude::*;

mod models;

mod forecast;
mod period;

use crate::{forecast::ForecastComponent, models::Forecast};

#[derive(PartialEq, Properties)]
pub struct ForecastDisplayProps {}

#[function_component]
pub fn ForecastDisplay(props: &ForecastDisplayProps) -> Html {
    let ForecastDisplayProps {} = props;
    let forecast_state: Box<UseStateHandle<Option<Forecast>>> =
        Box::new(use_state(|| None::<Forecast>));

    let error_state: Box<UseStateHandle<Option<String>>> = Box::new(use_state(|| None::<String>));

    fn fetch_data(
        forecast_state: Box<UseStateHandle<Option<Forecast>>>,
        error_state: Box<UseStateHandle<Option<String>>>,
    ) {
        wasm_bindgen_futures::spawn_local(async move {
            let forecase_endpoint = format!(
                "https://api.weather.gov/gridpoints/{office}/{x},{y}/forecast",
                office = "OKX",
                x = 37,
                y = 39,
            );
            let fetched_forecast = Request::get(&forecase_endpoint).send().await;

            match fetched_forecast {
                Ok(response) => {
                    let parsed_forecast: Result<Forecast, Error> = response.json().await;
                    match parsed_forecast {
                        Ok(forecast) => {
                            error_state.set(None);
                            forecast_state.set(Some(forecast));
                        }
                        Err(e) => {
                            error_state.set(Some(e.to_string()));
                        }
                    }
                }
                Err(e) => {
                    error_state.set(Some(e.to_string()));
                }
            }
        });
    }

    let retry = {
        let forecast_state = forecast_state.clone();
        let error_state = error_state.clone();

        Callback::from(move |_| {
            fetch_data(forecast_state.clone(), error_state.clone());
        })
    };

    match (*forecast_state).as_ref() {
        Some(f) => html! {
            <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-4 gap-4">
                <ForecastComponent forecast={f.clone()}/>
            </div>
        },
        None => match (*error_state).as_ref() {
            Some(e) => {
                html! {
                    <>
                    {"error"} {e.to_string()}
                    <button onclick={retry}>{"retry"}</button>
                    </>
                }
            }
            None => {
                fetch_data(forecast_state, error_state);
                html! {
                <div class="flex flex-col gap-4">
                    <p>
                        {"No Data yet"}
                    </p>
                    <div>
                        <button
                            class="rounded-md bg-white px-2.5 py-1.5 text-sm font-semibold text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 hover:bg-gray-50"
                            onclick={retry}
                        >{"Call api"}</button>
                    </div>
                </div>
                }
            }
        },
    }
}

#[function_component(App)]
fn app_component() -> Html {
    html! {
        <div class="p-8 flex flex-col gap-4">

            <h2 class="font-bold text-xl">{"Weather in New York"}</h2>
            <ForecastDisplay />
        </div>
    }
}

fn main() {
    yew::Renderer::<App>::new().render();
}
